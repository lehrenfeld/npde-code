from ngsolve import *
from netgen.occ import unit_square
from math import pi
import matplotlib.pyplot as plt
import numpy 

ngsglobals.msg_level = 0

mesh = Mesh (unit_square.GenerateMesh(maxh=0.25))
def Solve(order=1, ndof_max=50000):
    V = H1(mesh, order=order,
           dirichlet="left|top|bottom|right")
    if V.ndof > ndof_max:
        return None

    ### TODO: Compute Galerkin solution and calculate the error
    
    return #gather computed quantities here

# h-refinement:
nhrefs = 7 #number of h-refinements
for i in range(nhrefs):
    if i>0:
        mesh.Refine()
    results = Solve(order=3)
    #TODO: gather results for later plotting

# TODO:
# * add p-refinements above
#     (advice: do this before the h-refinement as the mesh can stay unaffected)
# * plot or export data for plotting
