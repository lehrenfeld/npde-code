from ngsolve import *
from ngsolve.meshes import *
import matplotlib.pyplot as plt
import numpy as np
from scipy.integrate import fixed_quad

space_refs = 4

mesh = Make1DMesh(n=2**(space_refs))
fes = H1(mesh, order=3, dirichlet=[1,2])

print(fes.ndof, fes.FreeDofs())
# define trial- and test-functions
u = fes.TrialFunction()
v = fes.TestFunction()

exact = sin(pi*x) #x*(1-x)
f_exact = pi**2*sin(pi*x) #2.

# the right hand side
f = LinearForm(fes)
f += f_exact * v * dx

# the bilinear-form 
a = BilinearForm(fes, symmetric=True)
a += grad(u)*grad(v)*dx

a.Assemble()
f.Assemble()

# the solution field
gfu = GridFunction(fes)
gfu.vec.data = a.mat.Inverse(fes.FreeDofs(), inverse="sparsecholesky") * f.vec

print ("L2-error:", sqrt (Integrate ( (gfu-exact)**2, mesh, order=8)))

small_h = 1./(3*2**space_refs)

def eval_basis_fun(j,x):
    xj = (j+1)*small_h
    other_xm = []
    if j %3 == 0:
        if x >= j*small_h and x <= (j+3)*small_h:
            other_xm = [j*small_h, (j+2)*small_h, (j+3)*small_h]
    elif j %3 == 1:
        if x >= (j-1)*small_h and x <= (j+2)*small_h:
            other_xm = [(j-1)*small_h, (j)*small_h, (j+2)*small_h]
    elif j %3 == 2:
        if x >= (j-2)*small_h and x <= (j+1)*small_h:
            other_xm = [(j-2)*small_h, (j-1)*small_h, (j)*small_h]
        elif x >= (j+1)*small_h and x <= (j+4)*small_h:
            other_xm = [(j+2)*small_h, (j+3)*small_h, (j+4)*small_h]
    if other_xm == []:
        return 0
    else:
        prod = 1
        for xm in other_xm:
            prod *= (x-xm)/(xj - xm)
        return prod

def eval_deriv_basis_fun(j,x):
    xj = (j+1)*small_h
    other_xm = []
    if j %3 == 0:
        if x >= j*small_h and x <= (j+3)*small_h:
            other_xm = [j*small_h, (j+2)*small_h, (j+3)*small_h]
    elif j %3 == 1:
        if x >= (j-1)*small_h and x <= (j+2)*small_h:
            other_xm = [(j-1)*small_h, (j)*small_h, (j+2)*small_h]
    elif j %3 == 2:
        if x >= (j-2)*small_h and x <= (j+1)*small_h:
            other_xm = [(j-2)*small_h, (j-1)*small_h, (j)*small_h]
        elif x >= (j+1)*small_h and x <= (j+4)*small_h:
            other_xm = [(j+2)*small_h, (j+3)*small_h, (j+4)*small_h]
    if other_xm == []:
        return 0
    else:
        prod = 1
        for xm in other_xm:
            prod *= (x-xm)/(xj - xm)
        return prod*sum([1/(x-xm) for xm in other_xm ])

ndofs = 2**space_refs*3-1
f = np.zeros( ndofs )
A = np.zeros( (ndofs, ndofs) )
for j in range(ndofs):
    for elem in range(2**space_refs):
        f[j] += fixed_quad( lambda y: [eval_basis_fun(j, yi)*pi**2*sin(pi*yi) for yi in y],elem/(2**space_refs), (elem+1)/(2**space_refs), n=3 )[0] # 2
for i in range(ndofs):
    for j in range(ndofs):
        for elem in range(2**space_refs):
            A[i,j] += fixed_quad( lambda y: [eval_deriv_basis_fun(j, yi)*eval_deriv_basis_fun(i, yi) for yi in y],elem/(2**space_refs), (elem+1)/(2**space_refs), n=3 )[0]

sol = np.linalg.solve(A,f)
def eval_sol(x):
    s = 0
    for i in range(ndofs):
        s += sol[i]*eval_basis_fun(i,x)
    return s

l2sum = 0
for elem in range(2**space_refs):
    l2sum += fixed_quad( lambda y: [ (eval_sol(yi) - sin(pi*yi) )**2 for yi in y],elem/(2**space_refs), (elem+1)/(2**space_refs), n=8 )[0] #yi*(1-yi)
print ("L2-error:", sqrt (l2sum))
# # make data
# x = np.linspace(0, 1, 100)
# 
# # plot
# fig, ax = plt.subplots()
# ax.plot(x, [ eval_sol(xi) for xi in x])
# plt.show()
