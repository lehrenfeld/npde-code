# solve the Convection diffusion equation - eps Delta u + w · grad u = f
# with Dirichlet boundary condition u = 0 and a specific r.h.s. f

from ngsolve import *
from netgen.occ import *
from netgen import gui
ngsglobals.msg_level = 1

# generate a triangular mesh of mesh-size 0.1
maxh = 0.1
mesh = Mesh( unit_square.GenerateMesh(maxh=maxh))

w = (1,1)
wind = CoefficientFunction(w)
eps = 0.1

### exact solution
p = lambda x: x + (exp(-w[0]*(1-x)/eps)-exp(-w[0]/eps))/(exp(-w[0]/eps)-1)
q = lambda y: y + (exp(-w[1]*(1-y)/eps)-exp(-w[1]/eps))/(exp(-w[1]/eps)-1)
exact = p(x) * q(y)
### r.h.s. f:
coef_f =  w[1] * p(x) +  w[0] * q(y)

# H1-conforming finite element space
# Note that the Dirichlet boundary conditions are implemented into the space!
V = H1(mesh, order=2, dirichlet="left|right|bottom|top")
u,v = V.TrialFunction(), V.TestFunction()

h = specialcf.mesh_size

laplace = lambda u : Trace(u.Operator("hesse"))

a = BilinearForm (V, symmetric=False)
a += eps * grad(u) * grad(v) * dx + wind * grad(u) * v * dx 

f = LinearForm (V)
f += coef_f * v * dx

a.Assemble()
f.Assemble()

# the solution field 
gfu = GridFunction (V)
# solving the arising linear system
f.vec.data -= a.mat * gfu.vec
gfu.vec.data += a.mat.Inverse(V.FreeDofs(), inverse="umfpack") * f.vec

# plot the solution (netgen-gui only)
Draw (gfu)

print("             L2 norm Error =", sqrt(Integrate((gfu-exact)**2,mesh)))
print("         H1 seminorm Error =", sqrt(Integrate((Norm(grad(gfu)-CoefficientFunction((exact.Diff(x),exact.Diff(y))) ) )**2,mesh)))
print("streamline derivatie Error =", sqrt(Integrate((wind*(grad(gfu)-CoefficientFunction((exact.Diff(x),exact.Diff(y))) ) )**2,mesh)))

delta = 1.5*maxh
cutoff = IfPos(1-delta-x,1,0) * IfPos(1-delta-y,1,0)
print("streamline derivatie Error (cutoff) =", sqrt(Integrate(cutoff*(wind*(grad(gfu)-CoefficientFunction((exact.Diff(x),exact.Diff(y))) ) )**2,mesh)))
