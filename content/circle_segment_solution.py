from ngsolve import *
from netgen.geom2d import SplineGeometry
from math import pi
ngsglobals.msg_level = 0
from circle_segment_geom import MakeCircleSegmentGeometry

def Solve(q,nref):
    alpha = pi/2*q
    geo = MakeCircleSegmentGeometry(q)
    mesh = Mesh( geo.GenerateMesh(maxh=0.1))
    V = H1(mesh,order=2, dirichlet="slit|circle", autoupdate=True)
    u,v = V.TrialFunction(), V.TestFunction()

    a = BilinearForm (V, symmetric=True, autoupdate=True)
    a += grad(u) * grad(v) * dx
    
    #c = Preconditioner(a, type="direct", inverse="sparsecholesky", autoupdate=True)

    f = LinearForm (V, autoupdate=True)
    gfu = GridFunction (V, autoupdate=True)
    
    phi = pi/2-atan2(x-1e-14,y)
    r = sqrt(x**2+y**2)

    exact = sin(phi/alpha*pi)*r**(pi/alpha)
    error = gfu - exact

    exactdudx = exact.Diff(x)
    exactdudy = exact.Diff(y)
    exact_grad = (exactdudx,exactdudy)
    error_grad = grad(gfu) - exact_grad
    
    Draw(exact_grad, mesh, "exact_gradu")
    Draw(exact, mesh, "exact_u")
    Draw(gfu,mesh,"u")
    Draw(grad(gfu), mesh, "gradu")
    
    errors = []
    for i in range(nref):
        if i>0:
            mesh.Refine()
    
        a.Assemble()
        f.Assemble()
        
        gfu.Set(sin(phi/alpha*pi),definedon=mesh.Boundaries("circle"))
        
        f.vec.data -= a.mat * gfu.vec
        gfu.vec.data += a.mat.Inverse(freedofs=V.FreeDofs()) * f.vec
        
        l2err = sqrt(Integrate(error**2,mesh))
        h1err = sqrt(l2err**2 + Integrate(InnerProduct(error_grad,error_grad),mesh))
        print("l2 error:{:14.8f}, h1 error:{:14.8f}".format(l2err,h1err), end="")
        errors.append((l2err,h1err))
        if i > 0:
            print(", ratio l2: {:6.2f}, ratio h1: {:6.2f}".format(errors[-2][0]/errors[-1][0], errors[-2][1]/errors[-1][1]), end="")
        print("")
    return 

for q in [1,2,3]:
    print("--  q="+str(q)+"  --")
    Solve(q=q,nref=5)
    print("")
print("-----------")
